ProcessStartInfo startInfo = new ProcessStartInfo();
startInfo.CreateNoWindow = false;
startInfo.UseShellExecute = false;
startInfo.FileName = AxiomHome + @"\sys\exe\ttyaxiom.exe";
startInfo.WindowStyle = ProcessWindowStyle.Normal;
startInfo.Arguments = reportName;
startInfo.WorkingDirectory = this.folderBrowserDialog1.SelectedPath;

try
{
	using (Process exeProcess = Process.Start(startInfo))
	{
        	exeProcess.WaitForExit();
        }
}
catch
{
}