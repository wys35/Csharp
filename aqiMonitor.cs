using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;


namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            int warningThreshold = 0;
            int aqiIndex = 0;
            String errorMessage = "";

            // Initialize, check for threshold
            if (args.Length == 0)
            {
                errorMessage = "Warning email threshold not specified";
                return;
            } else
            {
                warningThreshold = Int32.Parse(args[0]);
            }


            // Read PM reading for Suzhou and parse

            String respHttp;
            WebRequest wReq = WebRequest.Create("http://www.pm25.in/suzhou");
            try
            {
                WebResponse wResp = wReq.GetResponse();
                Stream respStream = wResp.GetResponseStream();
                using (StreamReader reader = new StreamReader(respStream, Encoding.UTF8))
                {
                    respHttp = reader.ReadToEnd();
                    respStream.Close();
                    wResp.Close();
                }
                // Parse Html
                HtmlAgilityPack.HtmlDocument htmlDoc = new HtmlAgilityPack.HtmlDocument();
                htmlDoc.LoadHtml(respHttp);

                if (htmlDoc.DocumentNode != null)
                {
                    try
                    {
                        HtmlAgilityPack.HtmlNode span12dataNode = htmlDoc.DocumentNode.SelectSingleNode("//div[contains(@class, 'span12') and contains(@class, 'data')]");
                        HtmlAgilityPack.HtmlNode aqiNode = span12dataNode.SelectSingleNode("div[1] / div[1]");
                        aqiIndex = Int32.Parse(aqiNode.InnerText);
                    }
                    catch (Exception XpathE)
                    {
                        errorMessage = XpathE.ToString();
                    }
                }
                else
                {
                    errorMessage = "Web Server Down";
                }
            } catch (Exception e)
            {
                errorMessage = e.ToString();
            }



            // Construct SMTP client (both warning and debug)
            SmtpClient client = new SmtpClient("smtp.office365.com");
            client.Port = 25;
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            NetworkCredential cred = new System.Net.NetworkCredential("name@company.com", "password");
            client.Credentials = cred;

            // Warning email
            if (aqiIndex >= warningThreshold && errorMessage == "")
            {
                Console.WriteLine("Warning - Current Air Quality Index: " + aqiIndex.ToString());
                // Construct Email Message
                MailMessage mail = new MailMessage();

                ContentType textType = new ContentType("text/plain");
                ContentType HTMLType = new ContentType("text/html");
                ContentType calendarType = new ContentType("text/calendar");

                calendarType.Parameters.Add("method", "REQUEST");
                calendarType.Parameters.Add("name", "meeting.ics");

                // Create message body parts
                string organizerName = "Name";
                string location = "Location";
                string summary = "Dear Team: \r\n";
                summary += "Current Suzhou Air Quality Index is " + aqiIndex.ToString() + ". You'd better wear a mask when you're outdoor since the PM2.5 today shows serious air pollution. Take care.";
                string organizerEmail = "name@company.com";
                string attendeeList = "attendee@company.com";
                DateTime start = DateTime.Now;
                DateTime end = DateTime.Now.AddHours(11);

                // create the Body in text format
                string bodyText = "Type:Single Meeting\r\n";
                bodyText += "Organizer: {0}\r\n";
                bodyText += "Start Time:{1}\r\n";
                bodyText += "End Time:{2}\r\n";
                bodyText += "Time Zone:{3}\r\n";
                bodyText += "Location: {4}\r\n\r\n";
                bodyText += "*~*~*~*~*~*~*~*~*~*\r\n\r\n{5}";
                bodyText = string.Format(bodyText,
                    organizerName,
                    start.ToLongDateString() + " " + start.ToLongTimeString(),
                    end.ToLongDateString() + " " + end.ToLongTimeString(),
                    System.TimeZone.CurrentTimeZone.StandardName,
                    location,
                    summary);
                AlternateView textView = AlternateView.CreateAlternateViewFromString(bodyText, textType);
                mail.AlternateViews.Add(textView);

                // create the Body in HTML format
                string bodyHTML = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2//EN\">\r\n";
                bodyHTML += "<HTML>\r\n";
                bodyHTML += "<HEAD>\r\n";
                bodyHTML += "<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=utf-8\">\r\n";
                bodyHTML += "<META NAME=\"Generator\" CONTENT=\"MS Exchange Server version 6.5.7652.24\">\r\n";
                bodyHTML += "<TITLE>{0}</TITLE>\r\n";
                bodyHTML += "</HEAD>\r\n";
                bodyHTML += "<BODY>\r\n";
                bodyHTML += "<!-- Converted from text/plain format -->\r\n";
                bodyHTML += "<P><FONT SIZE=2>Single Meeting<BR>\r\n";
                bodyHTML += "Organizer:{1}<BR>\r\n";
                bodyHTML += "Start Time:{2}<BR>\r\n";
                bodyHTML += "End Time:{3}<BR>\r\n";
                bodyHTML += "Time Zone:{4}<BR>\r\n";
                bodyHTML += "Location:{5}<BR>\r\n";
                bodyHTML += "<BR>\r\n";
                bodyHTML += "*~*~*~*~*~*~*~*~*~*<BR>\r\n";
                bodyHTML += "<BR>\r\n";
                bodyHTML += "</FONT><FONT SIZE=3>Dear Team: <BR>\r\n";
                bodyHTML += "<BR>\r\n";
                bodyHTML += "Current Suzhou Air Quality Index is " + aqiIndex.ToString() + ". You'd better wear a mask when you're outdoor since the PM2.5 today shows serious air pollution. Take care.\r\n";
                bodyHTML += "</FONT>\r\n";
                bodyHTML += "</P>\r\n";
                bodyHTML += "\r\n";
                bodyHTML += "</BODY>\r\n";
                bodyHTML += "</HTML>";
                bodyHTML = string.Format(bodyHTML,
                    "Air Quality Warning",
                    organizerName,
                    start.ToLongDateString() + " " + start.ToLongTimeString(),
                    end.ToLongDateString() + " " + end.ToLongTimeString(),
                    System.TimeZone.CurrentTimeZone.StandardName,
                    location);

                AlternateView HTMLView = AlternateView.CreateAlternateViewFromString(bodyHTML, HTMLType);
                mail.AlternateViews.Add(HTMLView);

                // create the Body in VCALENDAR format
                string calDateFormat = "yyyyMMddTHHmmssZ";
                string bodyCalendar = "BEGIN:VCALENDAR\r\n";
                bodyCalendar += "METHOD:REQUEST\r\n";
                bodyCalendar += "PRODID:Microsoft CDO for Microsoft Exchange\r\n";
                bodyCalendar += "VERSION:2.0\r\n";
                bodyCalendar += "BEGIN:VTIMEZONE\r\n";
                bodyCalendar += "TZID:(GMT+08.00) China Standard Time\r\n";
                bodyCalendar += "X -MICROSOFT-CDO-TZID:45\r\n";
                bodyCalendar += "BEGIN:STANDARD\r\n";
                bodyCalendar += "DTSTART:16010101T020000\r\n";
                bodyCalendar += "TZOFFSETFROM:+0800\r\n";
                bodyCalendar += "TZOFFSETTO:+0800\r\n";
                bodyCalendar += "RRULE:FREQ=YEARLY;WKST=MO;INTERVAL=1;BYMONTH=11;BYDAY=1SU\r\n";
                bodyCalendar += "END:STANDARD\r\n";
                bodyCalendar += "BEGIN:DAYLIGHT\r\n";
                bodyCalendar += "DTSTART:16010101T020000\r\n";
                bodyCalendar += "TZOFFSETFROM:+0800\r\n";
                bodyCalendar += "TZOFFSETTO:+0800\r\n";
                bodyCalendar += "RRULE:FREQ=YEARLY;WKST=MO;INTERVAL=1;BYMONTH=3;BYDAY=2SU\r\n";
                bodyCalendar += "END:DAYLIGHT\r\n";
                bodyCalendar += "END:VTIMEZONE\r\n";
                bodyCalendar += "BEGIN:VEVENT\r\n";
                bodyCalendar += "DTSTAMP:{8}\r\n";
                bodyCalendar += "DTSTART:{0}\r\n";
                bodyCalendar += "SUMMARY:{7}\r\n";
                bodyCalendar += "UID:{5}\r\n";
                bodyCalendar += "ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE;CN=\"{9}\":MAILTO:{9}\r\n";
                bodyCalendar += "ACTION;RSVP=TRUE;CN=\"{4}\":MAILTO:{4}\r\n";
                bodyCalendar += "ORGANIZER;CN=\"{3}\":mailto:{4}\r\n";
                bodyCalendar += "LOCATION:{2}\r\n";
                bodyCalendar += "DTEND:{1}\r\n";
                bodyCalendar += "DESCRIPTION:{7}\\N\r\n";
                bodyCalendar += "SEQUENCE:1\r\n";
                bodyCalendar += "PRIORITY:5\r\n";
                bodyCalendar += "CLASS:\r\n";
                bodyCalendar += "CREATED:{8}\r\n";
                bodyCalendar += "LAST -MODIFIED:{8}\r\n";
                bodyCalendar += "STATUS:CONFIRMED\r\n";
                bodyCalendar += "TRANSP:OPAQUE\r\n";
                bodyCalendar += "X -MICROSOFT-CDO-BUSYSTATUS:FREE\r\n";
                bodyCalendar += "X -MICROSOFT-CDO-INSTTYPE:0\r\n";
                bodyCalendar += "X -MICROSOFT-CDO-INTENDEDSTATUS:FREE\r\n";
                bodyCalendar += "X -MICROSOFT-CDO-ALLDAYEVENT:FALSE\r\n";
                bodyCalendar += "X -MICROSOFT-CDO-IMPORTANCE:1\r\n";
                bodyCalendar += "X -MICROSOFT-CDO-OWNERAPPTID:-1\r\n";
                bodyCalendar += "X -MICROSOFT-CDO-ATTENDEE-CRITICAL-CHANGE:{8}\r\n";
                bodyCalendar += "X -MICROSOFT-CDO-OWNER-CRITICAL-CHANGE:{8}\r\n";
                bodyCalendar += "BEGIN:VALARM\r\n";
                bodyCalendar += "ACTION:DISPLAY\r\n";
                bodyCalendar += "DESCRIPTION:REMINDER\r\n";
                bodyCalendar += "TRIGGER;RELATED=START:-PT00H15M00S\r\n";
                bodyCalendar += "END:VALARM\r\n";
                bodyCalendar += "END:VEVENT\r\n";
                bodyCalendar += "END:VCALENDAR\r\n";

                bodyCalendar = string.Format(bodyCalendar,
                    start.ToUniversalTime().ToString(calDateFormat),
                    end.ToUniversalTime().ToString(calDateFormat),
                    location,
                    organizerName,
                    organizerEmail,
                    Guid.NewGuid().ToString("B"),
                    summary,
                    "Air Quality Warning",
                    DateTime.Now.ToUniversalTime().ToString(calDateFormat),
                    attendeeList.ToString());

                AlternateView calendarView = AlternateView.CreateAlternateViewFromString(bodyCalendar, calendarType);
                calendarView.TransferEncoding = TransferEncoding.SevenBit;
                mail.AlternateViews.Add(calendarView);

                // Warning email
                mail.From = new MailAddress("name@company.com", "Display Name");
                mail.To.Add("names@company.com");


                try
                {
                    client.Send(mail);
                    Console.WriteLine("Warning email sent.");
                } catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            } else
            {
                Console.WriteLine("AQI below warning level, no warning email sent.");
            }

            // Debug email
            MailMessage debugMail = new MailMessage();
            debugMail.From = new MailAddress("name@company.com", "Display Name");
            debugMail.To.Add("name@company.com");
            debugMail.To.Add("name@company.com");
            debugMail.IsBodyHtml = true;
            debugMail.Subject = "Air Quality Index";
            debugMail.Body = "<html><body><p><h1>Current Suzhou Air Quality Index is " + aqiIndex.ToString();
            debugMail.Body += "</h1></p><p></p>";
            if (errorMessage != "")
            {
                debugMail.Body += "Error message: ";
            }
            debugMail.Body += errorMessage;
            debugMail.Body += "</body></html>";

            try
            {
                client.Send(debugMail);
                Console.WriteLine("Debug email sent");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            //
            Console.ReadLine();
        }
    }
}
