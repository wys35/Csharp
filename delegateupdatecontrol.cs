public delegate void invokeDelegate();

public void updateTasks()
        {
            if (this.textBox1.InvokeRequired) { 
                this.textBox1.Invoke(new invokeDelegate(updateTasks), new object[] { });
            } else {
                textBox1.Clear();
                List<string> temp = ax1.getRunningReports();
                foreach (string rpt in temp) {
                    this.textBox1.Text += rpt + Environment.NewLine;
                }
            }
        }

private void tcpServer1_OnConnect(tcpServer.TcpServerConnection connection)
        {
            invokeDelegate setText = () => this.label2.Text = tcpServer1.Connections.Count.ToString();
            Invoke(setText);
        }